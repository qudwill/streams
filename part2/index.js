'use strict';

const { Transform } = require('stream');
const crypto = require('crypto');
const fs = require('fs');
const input = fs.createReadStream('input.txt');
const output = fs.createWriteStream('output.txt');

class CreateHashTransform extends Transform {
	constructor(options) {
		super(options);
	}

	_transform(chunk, encoding, callback) {
		this.push(crypto.createHash('md5').update(chunk.toString('utf8')).digest('hex'));

		callback();
	}
}

input
	.pipe(new CreateHashTransform())
	.pipe(output);