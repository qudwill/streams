'use strict';

const fs = require('fs');
const crypto = require('crypto');
const input = fs.createReadStream('input.txt');
const output = fs.createWriteStream('output.txt');

input.on('readable', function() {
	while (data = input.read()) {
		let hash = crypto.createHash('md5').update(data.toString('utf8')).digest('hex');
		
		console.log(hash);
		output.write(hash);
	};
});