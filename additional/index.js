'use strict';

const { Readable, Writable, Transform } = require('stream');
const transformFreq = 1000;

const randomNumeral = () => {
	return Math.floor(Math.random() * 10);
}

class CReadable extends Readable {
  constructor(options) {
    super(options);
  }

	_read() {
		this.push('' + randomNumeral());
	}
}

class CWritable extends Writable {
	constructor(options) {
		super(options);
	}

	_write(chunk, encoding, done) {
		console.log(chunk.toString());

		done();
	}
}

class CTransform extends Transform {
  constructor(options) {
    super(options);
  }

  _transform(chunk, encoding, done) {
    const interval = setInterval(() => {
			console.log('sending data from CTransform');
	
    	this.push('{value: \'' + chunk + '\'}');

   		done();
			clearInterval(interval);
		}, transformFreq);
  }
}

const r = new CReadable();
const w = new CWritable('w');
const t = new CTransform('t');

r
	.pipe(t)
	.pipe(w);
